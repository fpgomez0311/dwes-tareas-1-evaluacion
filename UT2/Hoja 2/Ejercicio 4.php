<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>
<body>
    <?php
    $nombre = "Juan";
    echo "Información de la variable 'nombre': ";
    echo var_dump($nombre) . "<br>";
    echo "Contenido de la variable: " .$nombre . "<br>";
    $nombre = null;
    echo "Después de asignarle un valor nulo: " .gettype($nombre);

    ?>
</body>
</html>