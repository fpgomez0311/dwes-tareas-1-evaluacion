<?php

require "funcionesMySQLI.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Realizar una reserva</title>
</head>
<body>
<h2>Reserva de asiento</h2>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre"><br>
    <label for="DNI">DNI:</label>
    <input type="text" min="9" max="9" id="DNI" name="DNI"><br>
    <label for="asiento">Asiento:</label>
    <select name="asiento">
    <?php
      $asientos = obtenerAsientos();
      foreach($asientos as $asiento){
          foreach($asiento as $numero => $precio){
              echo "<option value='".$numero."'>(".$numero.") ".$precio."</option>";
          }
      }
    ?>
    </select>
    <br>
    <input type="submit" id="reservar" name="reservar" value="Reservar">
    </form>
</body>
</html>
<?php
 if(isset($_POST["reservar"])){
     $dni = $_POST["DNI"];
     $nombre =$_POST["nombre"];
     $asiento = $_POST["asiento"];
     reservar($dni,$nombre,$asiento);
 }


?>