<?php

require 'funcionesMySQLI.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plazas</title>
</head>
<body>
    <h2>Gestión de plazas</h2>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    
    <?php
       $plazas = obtenerAsientos();
       foreach($plazas as $plaza){
           foreach($plaza as $numero => $precio){
               echo "<label for='plaza".$numero."'>Plaza ".$numero.": </label>";
               echo "<input type='hidden' name='antes[]' value='".$numero."'>";
               echo "<input type=number id='".$numero."' name='plazas[]' value='".$precio."'><br>";

           }
       }
    ?>
    <br>
    <input type="submit" id="actualizar" name="actualizar" value="Actualizar">
    </form>
</body>
</html>

<?php
if(isset($_POST["actualizar"])){
$anteriores = $_POST['antes'];
$contador = 0;
$nuevos = $_POST["plazas"];
for($i = 0; $i < count($nuevos);$i++){
    actualizar($anteriores[$i],$nuevos[$i]);
}
} 
 
 

?>