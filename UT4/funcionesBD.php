<?php 
require_once "conexionBBDD.php";

/*FUNCIONES GET*/

function getDiscos(){
	$mysqli=getConexionMysqli();
	$consulta="SELECT id_producto, titulo, autor, discografica, precio, imagen, descripcion, proveedores.nombre as proveedor, generos.nombre as genero from productos INNER JOIN generos ON productos.id_genero=generos.id_genero INNER JOIN proveedores ON productos.id_proveedor=proveedores.id_proveedor order by autor";
	if($resultado= $mysqli->query($consulta)){
		while($disco= $resultado->fetch_array()){
			$discos[]= array("id_producto"=>$disco["id_producto"],"titulo"=>$disco["titulo"], "autor"=>$disco["autor"], "discografica"=>$disco["discografica"], "precio"=>$disco["precio"], "imagen"=>$disco["imagen"], "descripcion"=>$disco["descripcion"], "genero"=>$disco["genero"], "proveedor"=>$disco["proveedor"]);
		}
		$resultado->close();
	}
	$mysqli->close();
	return $discos;
}

function getDiscosPorGenero($genero){
	$mysqli=getConexionMysqli();
	$consulta="SELECT id_producto, titulo, autor, discografica, precio, imagen, descripcion, proveedores.nombre as proveedor, generos.nombre as genero, telefono from productos INNER JOIN generos ON productos.id_genero=generos.id_genero INNER JOIN proveedores ON productos.id_proveedor=proveedores.id_proveedor where generos.nombre='$genero' order by autor";
	if($resultado= $mysqli->query($consulta)){
		while($disco= $resultado->fetch_array()){
			$discos[]= array("id_producto"=>$disco["id_producto"],"titulo"=>$disco["titulo"], "autor"=>$disco["autor"], "discografica"=>$disco["discografica"], "precio"=>$disco["precio"], "imagen"=>$disco["imagen"], "descripcion"=>$disco["descripcion"], "genero"=>$disco["genero"], "proveedor"=>$disco["proveedor"], "telefono"=>$disco["telefono"]);
		}
		$resultado->close();
	}
	$mysqli->close();
	return $discos;

}

function getDiscosPorTitulo($titulo){
	$mysqli=getConexionMysqli();
	$consulta="SELECT id_producto, titulo, autor, discografica, precio, imagen, descripcion, proveedores.nombre as proveedor, generos.nombre as genero, telefono from productos INNER JOIN generos ON productos.id_genero=generos.id_genero INNER JOIN proveedores ON productos.id_proveedor=proveedores.id_proveedor where titulo='$titulo'";
	if($resultado= $mysqli->query($consulta)){
		while($disco= $resultado->fetch_array()){
			$discos[]= array("id_producto"=>$disco["id_producto"],"titulo"=>$disco["titulo"], "autor"=>$disco["autor"], "discografica"=>$disco["discografica"], "precio"=>$disco["precio"], "imagen"=>$disco["imagen"], "descripcion"=>$disco["descripcion"], "genero"=>$disco["genero"], "proveedor"=>$disco["proveedor"], "telefono"=>$disco["telefono"]);
		}
		$resultado->close();
	}
	$mysqli->close();
	return $discos;

}

function getDiscosPorAutor($autor){
	$mysqli=getConexionMysqli();
	$consulta="SELECT id_producto, titulo, autor, discografica, precio, imagen, descripcion, proveedores.nombre as proveedor, generos.nombre as genero, telefono from productos INNER JOIN generos ON productos.id_genero=generos.id_genero INNER JOIN proveedores ON productos.id_proveedor=proveedores.id_proveedor where autor='$autor' order by titulo";
	if($resultado= $mysqli->query($consulta)){
		while($disco= $resultado->fetch_array()){
			$discos[]= array("id_producto"=>$disco["id_producto"],"titulo"=>$disco["titulo"], "autor"=>$disco["autor"], "discografica"=>$disco["discografica"], "precio"=>$disco["precio"], "imagen"=>$disco["imagen"], "descripcion"=>$disco["descripcion"], "genero"=>$disco["genero"], "proveedor"=>$disco["proveedor"], "telefono"=>$disco["telefono"]);
		}
		$resultado->close();
	}
	$mysqli->close();
	return $discos;

}

function getGeneros(){
	$mysqli=getConexionMysqli();
	$consulta="SELECT id_genero, nombre from generos";
	if($resultado= $mysqli->query($consulta)){
		while($genero= $resultado->fetch_array()){
			$generos[]= array("id_genero"=>$genero["id_genero"],"nombre"=>$genero["nombre"]);
		}
		$resultado->close();
	}
	$mysqli->close();
	return $generos;
}

function getProveedores(){
	$mysqli=getConexionMysqli();
	$consulta="SELECT id_proveedor, nombre from proveedores";
	if($resultado= $mysqli->query($consulta)){
		while($proveedor= $resultado->fetch_array()){
			$proveedores[]= array("id_proveedor"=>$proveedor["id_proveedor"],"nombre"=>$proveedor["nombre"]);
		}
		$resultado->close();
	}
	$mysqli->close();
	return $proveedores;

}

/*FUNCIONES DE USUARIOS*/

function registro($nombre, $nombre_completo, $contrasena,$email, $direccion, $telefono){
	$conexion = getConexionMysqli();
	$consulta = $conexion->stmt_init();
	$consulta->prepare("INSERT into usuarios(nombre, nombre_completo, password,email,direccion,telefono,fecha_registro, rol) VALUES (?,?,?,?,?,?,?,'user')");
	$campo1 = $nombre;
	$campo2 = $nombre_completo;
	$campo3 = md5($contrasena);
	$campo4 = $email;
	$campo5 = $direccion;
	$campo6 = $telefono;
	$campo7 = date('Y-m-d');
	$consulta->bind_param('sssssss',$campo1,$campo2,$campo3,$campo4,$campo5,$campo6, $campo7);
	$consulta->execute();
	$consulta->close();
	$conexion->close();
}

function registroAdmin($nombre, $nombre_completo, $contrasena,$email, $direccion, $telefono){
	$conexion = getConexionMysqli();
	$consulta = $conexion->stmt_init();
	$consulta->prepare("INSERT into usuarios(nombre, nombre_completo, password,email,direccion,telefono,fecha_registro, rol) VALUES (?,?,?,?,?,?,?,'admin')");
	$campo1 = $nombre;
	$campo2 = $nombre_completo;
	$campo3 = md5($contrasena);
	$campo4 = $email;
	$campo5 = $direccion;
	$campo6 = $telefono;
	$campo7 = date('Y-m-d');
	$consulta->bind_param('sssssss',$campo1,$campo2,$campo3,$campo4,$campo5,$campo6, $campo7);
	$consulta->execute();
	$consulta->close();
	$conexion->close();
}


function validarPassword($contrasena){
	$correcto = true;
	if (!preg_match('`[a-z]`',$contrasena)){
		$correcto = false;
		
	}
	if (!preg_match('`[A-Z]`',$contrasena)){
		$correcto = false;
	}
	if (!preg_match('`[0-9]`',$contrasena)){
		$correcto = false;
	}
	return $correcto;
}

function usuarioNuevo($usuario){
	$correcto = true;
	$conexion = getConexionMysqli();
	$consulta ="SELECT nombre from usuarios WHERE nombre='$usuario'";
	$resultado = $conexion->query($consulta);
	if($resultado){
		if($resultado->num_rows !== 0){
			$correcto = false;
		}
	}
	return $correcto;
}

function getUsuario($user, $pass){
	//$sql="SELECT id_usuario, nombre, nombre_completo, email, rol from usuarios where nombre= :user and password = :pass";
	//$consulta=$PDO->prepare($sql);
	//$consulta->bindParam(':user', $user);
	//$consulta->bindParam(':pass', $pass);
	//if($consulta->execute()){
		
	//	if($dato=$consulta->fetch(PDO::FETCH_BOTH)){

	//		$usuario= array("id_usuario"=>$dato["id_usuario"],"nombre"=>$dato["nombre"],"nombre_completo"=>$dato["nombre_completo"], "email"=>$dato["email"], "rol"=> $dato["rol"]);
	//	}
	//	unset($consulta);
	//}
	//unset($PDO);
	//return $usuario;
}


function login($usuario,$pass){
	$correcto = true;
	$conexion = getConexionMysqli();
	$consulta = $conexion->stmt_init();
	$consulta->prepare("SELECT nombre from usuarios WHERE nombre= ? AND password= ?");
	$campo1 = $usuario;
	$campo2 = md5($pass);
	$consulta->bind_param('ss',$campo1,$campo2);
	$consulta->execute();
	$consulta->store_result();
	$numfilas = $consulta->num_rows(); 
	if($numfilas== 0){
		$correcto = false;
	}
	$consulta->close();
	$conexion->close();
	return $correcto;
}



/*ADMINISTRACIÓN*/

function guardarDisco($titulo, $autor, $discografica, $precio, $imagen, $descripcion, $id_genero, $id_proveedor){
	$mysqli=getConexionMysqli();
	$todo_bien=true;
	$mysqli->autocommit(false);
	$consultaInsert = $mysqli->stmt_init();
	$sqlInsert= "INSERT INTO productos (titulo, autor, discografica, precio, imagen, descripcion, id_genero, id_proveedor) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

	$consultaInsert->prepare($sqlInsert);
	//print_r($consultaInsert);
	$consultaInsert->bind_param('sssissii', $titulo, $autor, $discografica, $precio, $imagen, $descripcion, $id_genero, $id_proveedor);

	if(!$consultaInsert->execute()){
		$todo_bien=false;
	}
	$consultaInsert->close();

	if ($todo_bien== true){
		
		$mysqli->commit();
		
	}else{
		$mysqli->rollback();
		print "<p>No se han podido realizar los cambios.</p>";
	}
}

function guardarGenero($nombre){
	$mysqli=getConexionMysqli();
	$todo_bien=true;
	$mysqli->autocommit(false);
	$consultaInsert = $mysqli->stmt_init();
	$sqlInsert= "INSERT INTO generos (nombre) VALUES (?)";

	$consultaInsert->prepare($sqlInsert);
	//print_r($consultaInsert);
	$consultaInsert->bind_param('s', $nombre);

	if(!$consultaInsert->execute()){
		$todo_bien=false;
	}
	$consultaInsert->close();

	if ($todo_bien== true){
		
		$mysqli->commit();
		
	}else{
		$mysqli->rollback();
		print "<p>No se han podido realizar los cambios.</p>";
	}
}

function guardarProveedor($nombre, $telefono){
	$mysqli=getConexionMysqli();
	$todo_bien=true;
	$mysqli->autocommit(false);
	$consultaInsert = $mysqli->stmt_init();
	$sqlInsert= "INSERT INTO proveedores (nombre, telefono) VALUES (?, ?)";

	$consultaInsert->prepare($sqlInsert);
	//print_r($consultaInsert);
	$consultaInsert->bind_param('ss', $nombre, $telefono);

	if(!$consultaInsert->execute()){
		$todo_bien=false;
	}
	$consultaInsert->close();

	if ($todo_bien== true){
		
		$mysqli->commit();
		
	}else{
		$mysqli->rollback();
		print "<p>No se han podido realizar los cambios.</p>";
	}
}

/*BUSCADOR*/

function buscar($busqueda){
	$mysqli=getConexionMysqli();
	if ($busqueda!=""){

		$trozos=explode(" ",$busqueda);
		$numero=count($trozos);
		if ($numero==1) {

			$cadbusca="SELECT  id_producto, titulo, autor, discografica, precio, imagen, descripcion, proveedores.nombre as proveedor, generos.nombre as genero, telefono from productos INNER JOIN generos ON productos.id_genero=generos.id_genero INNER JOIN proveedores ON productos.id_proveedor=proveedores.id_proveedor WHERE  titulo LIKE  '%$busqueda%' OR autor LIKE  '%$busqueda%' LIMIT 50";
		} elseif ($numero>1) {

			$cadbusca="SELECT  id_producto, titulo, autor, discografica, precio, imagen, descripcion,proveedores.nombre as proveedor, generos.nombre as genero, telefono, MATCH ( titulo, autor)
			AGAINST (  '$busqueda' ) AS Score FROM productos INNER JOIN generos ON productos.id_genero=generos.id_genero INNER JOIN proveedores ON productos.id_proveedor=proveedores.id_proveedor WHERE
			MATCH ( titulo, autor ) AGAINST (  '$busqueda' ) ORDER  BY Score DESC LIMIT 50";
		}
		
		$resultado=$mysqli->query($cadbusca);
		$count_results= $resultado->num_rows;
		if($count_results>0){
			while($disco=$resultado-> fetch_array())
			{
				$discos[]= array("id_producto"=>$disco["id_producto"], "titulo"=>$disco["titulo"], "autor"=>$disco["autor"], "discografica"=>$disco["discografica"], "precio"=>$disco["precio"], "imagen"=>$disco["imagen"], "descripcion"=>$disco["descripcion"], "genero"=>$disco["genero"], "proveedor"=>$disco["proveedor"], "telefono"=>$disco["telefono"]);
			}
			$resultado->close();
			return $discos;
		}else{
			echo "<div class='alert alert-danger'>
			<div class='container'>
			<div class='d-flex'>
			<div class='alert-icon'>
			<i class='ion-ios-information-circle-outline'></i>
			</div>
			<p class='mb-0 ml-2'><b>Error Alert:</b> No existen discos para esa búsqueda.</p>
			</div>
			<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
			<span aria-hidden='true'><i class='ion-ios-close'></i></span>
			</button>
			</div>
			</div>";

		}

		$mysqli->close();
		

	}
	
}

/*FUNCIONES PARA LA CESTA*/

function print_r2($val){
        echo '<pre>';
        print_r($val);
        echo  '</pre>';
}


function getProductoPorId($id){
	//$PDO=getConexionPDO('dwes_tienda');	
	//$sql="SELECT id_producto, titulo, autor, imagen, precio from productos where id_producto = :id";
	//$consulta=$PDO->prepare($sql);	
	//$consulta->bindParam(':id', $id);
	//if($consulta->execute()){		
	//	if($dato=$consulta->fetch(PDO::FETCH_BOTH)){
	//		$producto= array(
	//			"id_producto"=>$dato["id_producto"],
	//			"titulo"=>$dato["titulo"],
	//			"autor"=>$dato["autor"],
	//			"imagen"=>$dato["imagen"], 
	//			"precio"=>$dato["precio"]
	//		);
	//	}
	//	unset($consulta);
	//}
	//unset($PDO);
	//return $producto;
}


/*FUNCIONES PARA EL PERFIL DE USUARIO*/

function perfilUsuario($id){
	//$PDO=getConexionPDO('dwes_tienda');	
	//$sql="SELECT id_usuario, nombre, nombre_completo,password, email, direccion, telefono,rol from usuarios where id_usuario= :id";
	//$consulta=$PDO->prepare($sql);
	//$consulta->bindParam(':id', $id);
	//if($consulta->execute()){
		
		//if($dato=$consulta->fetch(PDO::FETCH_BOTH)){

		//	$usuario= array("id_usuario"=>$dato["id_usuario"],"nombre"=>$dato["nombre"],"nombre_completo"=>$dato["nombre_completo"],"contraseña"=>$dato["password"],"email"=>$dato["email"], "rol"=> $dato["rol"], "direccion"=>$dato["direccion"], "telefono"=>$dato["telefono"]);
		//}
	//	unset($consulta);
	//}
	//unset($PDO);
	//return $usuario;

}
function actualizarPerfil($id,$usuario,$email,$nombre,$contrasena,$direccion,$telefono){
	$conexion = getConexionMysqli();
	$consulta = $conexion->stmt_init();
	$consulta->prepare("UPDATE usuarios SET nombre=?, nombre_completo=?, password=?, email=?, direccion=?, telefono=? WHERE id_usuario=?");
	$campo1 = $usuario;
	$campo2 = $nombre;
	$campo3 = md5($contrasena);
	$campo4 = $email;
	$campo5 = $direccion;
	$campo6 = $telefono;
	$campo7 = $id;
	$consulta->bind_param('ssssssi',$campo1,$campo2,$campo3,$campo4,$campo5,$campo6,$campo7);
	$consulta->execute();
	$consulta->close();
	$conexion->close();

}
