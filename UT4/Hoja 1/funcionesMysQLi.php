<?php
include "conexionMysQLi.php";

function getEquipos(){

    $conexion = getConexionMysqli();
    if($resultado = $conexion->query('SELECT nombre FROM equipos')){
     while($equipo = $resultado->fetch_array()){
         $equipos[] = array("nombre" => $equipo["nombre"]);
     }
}
return $equipos;
}
function getJugadores($equipo){
    $conexion = getConexionMysqli();
    if($resultado = $conexion->query('SELECT nombre FROM jugadores WHERE nombre_equipo="'.$equipo.'"')){
     while($jugador = $resultado->fetch_array()){
         $jugadores[] =array("nombre" => $jugador["nombre"]);
     }
}
return $jugadores;
}
function getJugadoresPeso($equipo){
    $conexion = getConexionMysqli();
    if($resultado = $conexion->query('SELECT nombre,peso FROM jugadores WHERE nombre_equipo="'.$equipo.'"')){
     while($jugador = $resultado->fetch_array()){
         $jugadores[] =array($jugador["nombre"] => $jugador["peso"]);
     }
}
return $jugadores;
}
function obtenerCodigo($nombre){
    $conexion = getConexionMysqli();
    if($resultado = $conexion->query('SELECT codigo FROM jugadores WHERE nombre="'.$nombre.'"')){
        while($jugador = $resultado->fetch_array()){
            $busqueda = $jugador["codigo"];
        }
        return $busqueda;
}
}
function obtenerEquipo($nombre){
    $conexion = getConexionMysqli();
    if($resultado = $conexion->query('SELECT nombre_equipo FROM jugadores WHERE nombre="'.$nombre.'"')){
        while($jugador = $resultado->fetch_array()){
            $busqueda = $jugador["nombre_equipo"];
        }
        return $busqueda;
}
}
function obtenerFilas(){
    $conexion = getConexionMysqli();
    if($resultado = $conexion->query('SELECT max(codigo) as total from jugadores')){
        while($filas = $resultado->fetch_array()){
            $busqueda = $filas["total"];
        }
        return $busqueda;
}
}
function traspasar($baja,$nombre,$procedencia,$altura,$peso,$posicion){
    $correcto = true;
    $conexion = getConexionMysqli();
    $conexion->autocommit(false);
    $codigo = obtenerCodigo($baja);
    $equipo = obtenerEquipo($baja);
    /* Borramos el jugador */
    $sql = "DELETE from jugadores where nombre='.$baja.'";
    
    if ($conexion->query($sql) != true){

        $correcto = false;
    };
    /* Borramos las estadísticas a través del código del jugador */

    $sql = "DELETE from estadisticas where jugador='.$codigo.'";
    if ($conexion->query($sql) != true){
        
        $correcto = false;
    };
    /* Añadimos el nuevo jugador (no se puede usar el codigo del jugador borrado por la propia estructura de la base de datos) */
    $consultaInsert = $conexion->stmt_init();
    $codigo = obtenerFilas()+1;
    $sql = "INSERT INTO jugadores (codigo, nombre, procedencia, altura, peso, posicion, nombre_equipo) VALUES ($codigo,?,?,?,?,?,?)";
    $consultaInsert->prepare($sql);
    $consultaInsert->bind_param("ssddss", $nombre,$procedencia,$altura,$peso,$posicion,$equipo);
    
    if(!$consultaInsert->execute()){
         
		$correcto=false;
	}
    if($correcto == true){
           $conexion->commit();
           print "Los cambios se han realizado correctamente";
    }else{
        $conexion->rollback();
        print "Ha ocurrido un error";
    }
}
?>
