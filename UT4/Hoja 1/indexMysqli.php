<?php

require 'funcionesMysQLi.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NBA</title>
    <style>
    table{
        border: 1px solid black;
    }
    </style>
</head>
<body>
    <h1>Jugadores de la NBA</h1>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >

    <label for="equipos">Equipo:</label>
    <select name="equipos">
    <?php
     $equipos = getEquipos();
    
     foreach($equipos as $equipo){
        foreach($equipo as $indice =>$nombre){
            echo "<option value='".$nombre."'>".$nombre."</option>";
        }
    }
    ?>
    </select>
    <br><br>
    <input type="submit" id="mostrar" name="mostrar" value="Mostrar">
    </form>
<?php
if(isset($_POST["mostrar"])){
    $seleccion = $_POST["equipos"];
    
    $jugadores = getJugadoresPeso($seleccion);
    echo "<table><tr><td>NOMBRE</td><td>PESO</td></tr>";
    foreach($jugadores as $jugador){
        foreach($jugador as $nombre => $peso){
            echo "<tr><td>".$nombre."</td><td>".$peso."</td></tr>";
        }
    }
    echo "</table>";
    /* Ejercicio 7 */
    $jugadores = getJugadores($seleccion);
    echo "<form name='form' method='post' action=''>";
    echo "<h2>Baja y alta de jugadores</h2>";
    echo "<label for='jugadores'>Baja de jugador:</label>";
    echo "<select name='baja'>";
    foreach($jugadores as $jugador){
       foreach($jugador as $indice => $nombre){
        echo "<option value='".$nombre."'>".$nombre."</option>";
    }
}
    echo "</select>";
    echo "<br>";
    echo "<h4>Datos del nuevo jugador:</h4>";
    echo "<label for='nombre'>Nombre:</label>";
    echo "<input type='text' id='nombre' name='nombre'><br>";
    echo "<label for='procedencia'>Procedencia:</label>";
    echo "<input type='text' id='procedencia' name='procedencia'><br>";
    echo "<label for='altura'>Altura:</label>";
    echo "<input type='number' id='altura' name='altura'><br>";
    echo "<label for='peso'>Peso:</label>";
    echo "<input type='number' id='peso' name='peso'><br>";
    echo "<label for='posicion'>Posicion:</label>";
    echo "<select name='posicion'>";
    echo "<option value='F'>F</option>";
    echo "<option value='G'>G</option>";
    echo "<option value='G-F'>G-F</option>";
    echo "<option value='C'>C</option>";
    echo "<option value='C-F'>F-C</option>";
    echo "</select><br>";
    echo "<input type='submit' name='traspasar' id='traspasar' value='Realizar traspaso'>";
    echo "</form>";
}
if(isset($_POST['traspasar'])){
    $baja = $_POST["baja"];
    echo $baja;
    $nombre = $_POST["nombre"];
    $procedencia = $_POST["procedencia"];
    $altura = $_POST["altura"];
    $peso = $_POST["peso"];
    $posicion = $_POST["posicion"];
    $codigo = obtenerCodigo($baja);
    echo $codigo;
    $equipo = obtenerEquipo($baja);
    echo $equipo;
    traspasar($baja,$nombre,$procedencia,$altura,$peso,$posicion);
}
/* Ejercicio 8 */
?>
</body>
</html>