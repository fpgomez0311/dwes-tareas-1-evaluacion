<?php

require 'funciones.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Libros guardados</title>
    <style>
    table{
        border:1px solid black;
    }
    </style>  
</head>
<body>
    <table>
    <tr>
    <td>NÚMERO DE EJEMPLAR</td>
    <td>TÍTULO</td>
    <td>AÑO DE EDICIÓN</td>
    <td>PRECIO</td>
    <td>FECHA DE ADQUISICIÓN</td>
    </tr>
    <?php
      $libros = obtenerLibros();
      foreach($libros as $libro){
        echo "<tr>";
          foreach($libro as $indice => $valor){
              echo "<td>".$valor."</td>";
          }
        echo "</tr>";
      }

    ?>
    </table>
    <br>
<a Href="libros.php">Volver</a>
</body>
</html>