<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página principal de Libros</title>
</head>
<body>
    <form name="form" method="post" action="libros_guardar.php">
    <h1>INSERTE LOS DATOS DEL LIBRO</h1>
    <label for="titulo">Titulo:</label>
    <input type="text" id="titulo" name="titulo"><br>
    <label for="anio">Año de edición:</label>
    <input type="number" id="anio" name="anio"><br>
    <label for="precio">Precio:</label>
    <input type="number" id="precio" name="precio" step="any"><br>
    <label for="fecha">Fecha de Adquisición:</label>
    <input type="date" id="fecha" name="fecha"><br><br>
    <input type="submit" id="guardar" name="guardar" value="Guardar datos del libro"><br><br>
    _________<br>
    <a href="libros_datos.php">Mostrar los libros guardados.</a>
    </form>
</body>
</html>
 