<?php 

function conexionPDO(){
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
    $conexion = new PDO('mysql:host=localhost;dbname=dwes_02_libros', 'root','' , $opciones);
return $conexion;
}

function guardarLibro($titulo,$anio,$precio,$fecha){
$correcto = true;
$conexion = conexionPDO();
$conexion->beginTransaction();

$consulta=$conexion->prepare('INSERT into libros (titulo,anioedicion,precio,fecha_adquisicion) VALUES (:cm2,:cm3,:cm4,:cm5)');
$consulta->bindParam(":cm2",$titulo);
$consulta->bindParam(":cm3",$anio);
$consulta->bindParam(":cm4",$precio);
$consulta->bindParam(":cm5",$fecha);
if(!$consulta->execute()){
  $correcto = false;
}
if($correcto){
    $conexion->commit();
    print "Se ha almacenado el libro con éxito.";
}else{
    $conexion->rollback();
    print "Ha ocurrido un error al insertar el libro.";
}
}

function conexionMySQL(){
    $conexion = new mysqli("localhost", "root", "", "dwes_02_libros", 3306);
    $conexion->set_charset("utf8");
    $error = $conexion->connect_errno;
    if ($error != null)
    {
     print "<p>Se ha producido el error: $conexion->connect_error.</p>";
     exit();
    }
   return $conexion;
}

function obtenerLibros(){
  $conexion = conexionMySQL();
  if($resultado = $conexion->query('SELECT numejemplar,titulo,anioedicion,precio,fecha_adquisicion FROM libros')){
    while($libro = $resultado->fetch_array()){
        $libros[] = array("numejemplar"=> $libro["numejemplar"],"titulo" => $libro["titulo"],"año" => $libro["anioedicion"], "precio"=> $libro["precio"],"fecha" => $libro["fecha_adquisicion"]);
    }
    return $libros;
}

}
?>
