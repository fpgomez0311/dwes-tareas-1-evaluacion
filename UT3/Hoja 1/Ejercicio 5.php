<?php

echo "LOS NÚMEROS CAPICÚAS ENTRE 100 Y 999: <br><br>";
for($i = 100; $i <= 999;$i++){
    $string = (string)$i;
    $inverso = strrev($string);
 
    if($string == $inverso && $i % 10){
        echo $i ."<br>";
    }
}

$numero = 44478;
$inverso = 0;
$aux = $numero;

while( $aux != 0){
    $resto = $aux % 10;
    $inverso = $inverso * 10 + $resto;
    $aux = (int)($aux / 10);
}

if($numero == $inverso){
    echo "Es capicúa.";
}else{
    echo "No es capicúa.";
}

?>