<?php
$coches = array(
 "Seat" => array("Alhambra","Ibiza","Ateca","Arona","Tarraco","Mii"),
 "Opel" => array("Astra","Corsa","Mokka","Insignia","Crossland","Grandland X"),
 "Audi" => array("A3","A4","A6","Q5","R8","TT")
);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
    <style>
    table{
        border:1px solid black;
    }
    </style>
</head>
<body>
    <h1>Busca tu coche</h1>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <label for="marca">Marca: </label>
    <select name="marca">
    <?php
      foreach($coches as $marca => $modelos){
          echo "<option value='".$marca."'>".$marca."</option>";
      }
    ?>
    </select><br><br>
    <input type="submit" id="mostrar" name="mostrar" value="Mostrar">
  </form>
</body>
</html>
<?php

if(isset($_POST["mostrar"])){
    $busqueda = $_POST["marca"];
    echo "<form name='form2' method='post' action='?'>";
    echo "<table><tr>Coches</tr>";
    foreach($coches as $marca => $modelos){
      if($busqueda == $marca){
          for($i = 0; $i < count($modelos);$i++){
          echo "<tr><td><input type='text' id='nombres' name='nombres' value='".$modelos[$i]."'></td></tr>";
      }}
    }
    echo "</table>";
    echo "<input type='submit' id='actualizar' name='actualizar' value='Actualizar'>";
    echo "</form>";
}
if(isset($_POST["actualizar"])){
    $cochesactualizados = array($_POST["nombres"]);
    $marca = $_POST["marca"];
    $i = 0;
    foreach ($coches[$marca] as $cocheOriginal){
        if($cocheOriginal != $cochesactualizados[$i]){
            echo "<p>Se ha actualizado".$cocheOriginal." por ". $cochesactualizados[$i];
        }
    }

}

?>