<?php

$equipos = array(
    "FC Barcelona" => array("entrenador" => array("Koeman","https://upload.wikimedia.org/wikipedia/commons/4/42/Ronald_Koeman_%282014%29.jpg"), "jugadores" => array("Ter Stegen" => "https://upload.wikimedia.org/wikipedia/commons/7/70/Ter_Stegen_2019_03_17_2.jpg", "Piqué" => "https://upload.wikimedia.org/wikipedia/commons/f/fa/Gerard_Piqu%C3%A9_2017.jpg", "Griezmann" => "https://upload.wikimedia.org/wikipedia/commons/6/6e/FRA-ARG_%2810%29_%28cropped%29.jpg")),
    "Real Madrid" => array("entrenador" => array("Zidane","https://upload.wikimedia.org/wikipedia/commons/8/82/Zinedine_Zidane_by_Tasnim_01.jpg"), "jugadores" => array("Courtois" => "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Courtois_2018_%28cropped%29.jpg/245px-Courtois_2018_%28cropped%29.jpg", "Ramos" =>"https://upload.wikimedia.org/wikipedia/commons/4/43/Russia-Spain_2017_%286%29.jpg", "Hazard"=>"https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Eden_Hazard_2018.jpg/130px-Eden_Hazard_2018.jpg"))
)
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Equipos de fútbol</title>
    <style>
        table {
            border: 1px solid black;
        }
    </style>
</head>

<body>
    <h1>Elige un equipo</h1>
    <hr>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="equipo">Equipo:</label>
        <select id="equipo" name="equipo">
            <option value="todos">--TODOS--</option>
            <?php
            foreach ($equipos as $equipo => $datos) {
                echo "<option value='" . $equipo . "'>" . $equipo . "</option>";
            }
            ?>
        </select>
        <br>
        <label for="puesto">Puesto:</label>
        Entrenador <input type="radio" id="entrenador" name="miembro" value="Entrenador">
        Jugadores<input type="radio" id="jugadores" name="miembro" value="Jugadores">
        <br>
        <input type="submit" id="mostrar" name="mostrar" value="Mostrar">
    </form>
</body>

</html>

<?php

$contador = 0;

if (isset($_POST["mostrar"])) {
    $seleccion = $_POST["equipo"];

    echo "<table>";
    if ($seleccion == "todos") {
        echo "<tr>";
        foreach ($equipos as $equipo => $datos) {
            echo "<td>" . $equipo . "</td>";
        }
        echo "</tr>";
        if(isset($_POST["miembro"]) && $_POST["miembro"] == "Entrenador"){
            echo "<tr>";
            foreach($equipos as $equipo => $datos){
                foreach($datos as $indice => $valor){
                    if($indice == "entrenador"){
                        echo "<td>".$valor[0]."<br><img src='".$valor[1]."' width='200px' height='300px'></td>";

                    }
                }
            }
        echo "</tr>";}
        if(isset($_POST["miembro"]) && $_POST["miembro"] == "Jugadores"){
            echo "<tr>";
            foreach($equipos as $equipo => $datos){
                foreach($datos as $indice => $valor){
                    if($indice == "jugadores"){
                        foreach($valor as $jugador => $imagen){
                            $contador++;
                            echo "<td>".$jugador."<br><img src='".$imagen."' width='200px' height='300px'></td>";
                            if($contador % 3 == 0){
                                echo "</tr>";}

                            }
                        }}
                    }}
       }
    }
    foreach ($equipos as $equipo => $datos) {
        if ($seleccion == $equipo) {
            if (isset($_POST["miembro"]) && $_POST["miembro"] == "Entrenador") {
                echo "<tr><td>";
                foreach ($datos as $indice => $valor) {
                    if ($indice == "entrenador") {
                         
                         echo "<tr><td>".$valor[0]."<br><img src='".$valor[1]."' width='200px' height='300px'></td><tr>";
                }}
                echo "</td></tr>";
            } else if (isset($_POST["miembro"]) && $_POST["miembro"] == "Jugadores") {
                foreach ($datos as $indice => $valor) {
                    if ($indice == "jugadores") {
                        foreach($valor as $jugador => $imagen){
                            echo "<tr><td>".$jugador."<br><img src='".$imagen."' width='200px' height='300px'></td><tr>";

                        }
                    }
                }
            }
        }
    }

echo "</table>";
?>