<?php

$verbos = array("ar" => array("saltar", "sumar", "hablar"), "er" => array("comer","acceder","arder"), "ir" => array("compartir","subir","vivir"));

$todos = array();

foreach($verbos as $terminacion => $posibles){
     for($i = 0; $i < count($posibles);$i++){
         array_push($todos,$posibles[$i]);
     }
    }
$random = floor(rand(0,count($todos)-1));
$verboaleatorio = $todos[$random];
$terminaen = substr($verboaleatorio,-2,2);
$verboaconjugar = substr($verboaleatorio,0,strlen($verboaleatorio)-2);

echo "El verbo " . $verboaleatorio. " conjugado es: <br>";
if($terminaen == "ar"){
    echo "Yo ". $verboaconjugar."o"."<br>";
    echo "Tu ". $verboaconjugar."as"."<br>";
    echo "El ".$verboaconjugar."a"."<br>";
    echo "Nosotros ".$verboaconjugar."amos"."<br>";
    echo "Vosotros ".$verboaconjugar."ais"."<br>";
    echo "Ellos ".$verboaconjugar."an"."<br>";
}else if($terminaen == "er"){
    echo "Yo ". $verboaconjugar."o"."<br>";
    echo "Tu ". $verboaconjugar."es"."<br>";
    echo "El ".$verboaconjugar."e"."<br>";
    echo "Nosotros ".$verboaconjugar."emos"."<br>";
    echo "Vosotros ".$verboaconjugar."eis"."<br>";
    echo "Ellos ".$verboaconjugar."en"."<br>";
}else{
    echo "Yo ". $verboaconjugar."o"."<br>";
    echo "Tu ". $verboaconjugar."es"."<br>";
    echo "El ".$verboaconjugar."e"."<br>";
    echo "Nosotros ".$verboaconjugar."imos"."<br>";
    echo "Vosotros ".$verboaconjugar."is"."<br>";
    echo "Ellos ".$verboaconjugar."en"."<br>";

}




?>