<?php

$peliculas = array("El padrino","Testigo de cargo","Luces de la ciudad","Cadena perpetua","Pulp Fiction","El gran dictador","Tiempos modernos","El golpe","Ser o no ser","La vida es bella");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>
<form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <h1>Buscador de peliculas:</h1>
    <input type="text" id="nombre" name="nombre" value="<?php echo isset($_POST['nombre']) ? $_POST['nombre'] : '' ?>">
    <br>
    <input type="submit" id="buscar" name="buscar" value="Buscar">
</form>
</body>
</html>

<?php

if(isset($_POST["buscar"])){
  $busqueda = $_POST["nombre"];
  $verificar = false;
  for($i = 0; $i < count($peliculas); $i++){
      if($busqueda == $peliculas[$i]){
         $verificar = true;
  }
}
  if($verificar == true){
    echo "La pelicula " .$busqueda. " se ha encontrado.";
  }else{
    echo "No se ha encontrado la pelicula " . $busqueda;
  }
}
?>