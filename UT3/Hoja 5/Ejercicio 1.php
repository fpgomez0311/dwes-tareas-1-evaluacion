<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conversor de monedas</title>
</head>
<body>
<form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
<h1>Conversor de monedas</h1>
<label for="cantidad">Cantidad: <label>
<input type="number" id="cantidad" name="cantidad">
<br>
<label for="origen">Origen:</label>
<select name="origen">
<option value="EUR">euros</option>
<option value="LIB">libras</option>
<option value="USD">dolarés</option>
</select>
<br>
<label for="destino">Destino:</label>
<select name="destino">
<option value="EUR">euros</option>
<option value="LIB">libras</option>
<option value="USD">dolarés</option>
</select>
<br>
<input type="submit" id="convertir" name="convertir" value="Convertir">
</form></body>
</html>

<?php
if(isset($_POST["convertir"])){

    $cantidad = $_POST["cantidad"];
    $origen = $_POST["origen"];
    $destino = $_POST["destino"];
     
    if($origen == $destino){
         echo  "<br>" .$cantidad . " " . $origen . " son " . $cantidad . " " . $destino;
    }else if($origen == "EUR" && $destino == "LIB"){
        $resultado = $cantidad * 0.86;
        echo  "<br>" .$cantidad . " " . $origen . " son " . round($resultado,2) . " " . $destino;
    }else if($origen == "EUR" && $destino == "USD"){
        $resultado = $cantidad * 1.18;
        echo  "<br>" .$cantidad . " " . $origen . " son " . round($resultado,2) . " " . $destino;
    }else if($origen == "LIB" && $destino == "EUR"){
        $resultado = $cantidad * 1.17;
        echo  "<br>" .$cantidad . " " . $origen . " son " . round($resultado,2) . " " . $destino;
    }else if($origen == "LIB" && $destino == "USD"){
        $resultado = $cantidad * 1.37;
        echo  "<br>" .$cantidad . " " . $origen . " son " . round($resultado,2) . " " . $destino;
    }else if($origen == "USD" && $destino == "EUR"){
        $resultado = $cantidad * 0.85;
        echo  "<br>" .$cantidad . " " . $origen . " son " . round($resultado,2) . " " . $destino;
    }else if($origen == "USD" && $destino == "LIB"){
        $resultado = $cantidad * 0.73;
        echo  "<br>" .$cantidad . " " . $origen . " son " . round($resultado,2) . " " . $destino;
    }



}


?>