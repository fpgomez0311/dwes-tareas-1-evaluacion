<?php

$peliculas = array("El padrino","Testigo de cargo","Luces de la ciudad","Cadena perpetua","Pulp Fiction","El gran dictador","Tiempos modernos","El golpe","Ser o no ser","La vida es bella");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
    <style>
    table{
        border:1px solid black;
    }
    </style>
</head>
<body>
<form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <h1>Buscador de peliculas:</h1>
    <input type="text" id="nombre" name="nombre" value="<?php echo isset($_POST['nombre']) ? $_POST['nombre'] : '' ?>">
    <br>
    <input type="submit" id="buscar" name="buscar" value="Buscar">
</form>
</body>
</html>

<?php

if(isset($_POST["buscar"])){
    $encontradas = array();
  $busqueda = $_POST["nombre"];
  $contador = 0;
  for($i = 0; $i < count($peliculas); $i++){
      if(str_contains($peliculas[$i], $busqueda)){
         $contador++;
         array_push($encontradas,$peliculas[$i]);
  }
}
echo $contador . " películas encontradas para la búsqueda '".$busqueda."'";

echo "<table>";

for($i = 0; $i < count($encontradas);$i++){
    echo "<tr><td>";
    echo "<img src='Peliculas/".$encontradas[$i].".jpg' width='50px' height='100px'>";
    echo "</td><td>".$encontradas[$i]."</td></tr>";
}
echo "</table>";
}
?>