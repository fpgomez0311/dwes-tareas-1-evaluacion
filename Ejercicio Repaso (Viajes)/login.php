<?php

require 'funciones.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Login</title>
</head>
<body>
<h1>Iniciar sesión en la agencia</h1>
<form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="usuario">ID Cliente: </label>
    <input type="text" id="idcliente" name="idcliente"><br>
    <label for="pass">Contraseña: </label>
    <input type="password" id="pass" name="pass"><br>
    <br>
    <input type="submit" id="comprobar" name="comprobar" value="Iniciar sesión"><br><br>
    </form>
</body>
</html>

<?php

if(isset($_POST["comprobar"])){
    $idcliente = $_POST["idcliente"];
    $password = $_POST["pass"];

    if(iniciarSesion($idcliente,$password)){
        session_start();
        $datos = iniciarSesion($idcliente,$password);
        foreach($datos as $usuario){
            foreach($usuario as $indice => $valor){
            $_SESSION[$indice] = $valor; 
            }
        }
        header("Location: viajes.php");
 
    }
}

?>