<?php


function conexionPDO(){
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
    $conexion = new PDO('mysql:host=localhost;dbname=dwes_agencia', 'root','' , $opciones);
return $conexion;
}

function iniciarSesion($idcliente,$password){
$correcto = true;
$conexion = conexionPDO();
$conexion->beginTransaction();

$consulta=$conexion->prepare('SELECT id,nombre FROM clientes WHERE id=:cm1 and password=:cm2');
$consulta->bindParam(":cm1",$idcliente);
$consulta->bindParam(":cm2",$password);

if ($consulta->execute())
{
 while ($fila = $consulta->fetch())
 {
 $datos[] = array("id" => $fila['id'], "nombre" => $fila['nombre']);
 }
 if(empty($datos)){
     $correcto = false;
 }

  if($correcto){
      $conexion->commit();
      unset($conexion);
      return $datos;
  }else{
      $conexion->rollback();
      print "No se ha encontrado al usuario.";
      unset($conexion);
  }
}}

function getConexionMysqli(){
    $conexion = new mysqli("localhost", "root", "", "dwes_agencia", 3306);
    $conexion->set_charset("utf8");
    $error = $conexion->connect_errno;
    if ($error != null)
    {
     print "<p>Se ha producido el error: $conexion->connect_error.</p>";
     exit();
    }
   return $conexion;
}
function obtenerViajes(){
$conexion = getConexionMysqli();
$consulta = $conexion->stmt_init();
if ($stmt = $consulta->prepare("SELECT id, nombre, precio FROM viaje ORDER BY precio ASC")){
    $consulta->execute();
}

$consulta->bind_result($campo1, $campo2, $campo3);
while($consulta->fetch())
{
    
$viajes [] = array($campo1 => array($campo2 => $campo3));
}
$consulta->close();
return $viajes;

}
function reservarViaje($idcliente, $idviaje, $numpersonas){
$correcto = true;
$conexion = conexionPDO();
$conexion->beginTransaction();

$consulta=$conexion->prepare('INSERT into reservas (id_cliente,id_viaje,num_plazas) VALUES (:cm1,:cm2,:cm3)');
$consulta->bindParam(":cm1",$idcliente);
$consulta->bindParam(":cm2",$idviaje);
$consulta->bindParam(":cm3",$numpersonas);
if(!$consulta->execute()){
  $correcto = false;
}
if($correcto){
    $conexion->commit();
    print "Se ha realizado la reserva correctamente.";
}else{
    $conexion->rollback();
    echo "Ha ocurrido un error al realizar la reserva.";
}
}
function obtenerViajesCliente($idcliente){
    $conexion = getConexionMysqli();
    $consulta = $conexion->stmt_init();
    if ($stmt = $consulta->prepare("SELECT viaje.nombre, viaje.precio, reservas.num_plazas FROM viaje INNER JOIN reservas ON viaje.id=reservas.id_viaje WHERE reservas.id_cliente=$idcliente")){
        $consulta->execute();
    }
    
    $consulta->bind_result($campo1, $campo2,$campo3);
    while($consulta->fetch())
    {
        
    $viajes [] = array($campo1 => array($campo2 => $campo3));
    }
    $consulta->close();
    return $viajes;
    
    }


?>
