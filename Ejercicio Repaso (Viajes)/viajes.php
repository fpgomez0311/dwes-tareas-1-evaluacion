<?php

require 'funciones.php';
session_start();
if (!isset($_SESSION['id']))
{
    header("Location: login.php");
    die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de viajes</title>
    <style>
        table {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <h1>Bienvenido <?php echo $_SESSION['nombre']?></h1>
    <table>
    <tr>
        <td>Nombre</td>
        <td>Precio</td>
    </tr>
    <?php
    $viajes = obtenerViajes();
    print_r($viajes);
       foreach($viajes as $viaje){
           echo "<tr>";
           foreach($viaje as $info => $valor){
               foreach($valor as $nombre => $precio){
                   echo "<td>".$nombre."</td><td>".$precio."</td>";
               }
           }
         echo "</tr>"; 
    }
    
    

    ?>
    </table>
    <br>
    <br>
    <a href="reservar.php">Reservar nuevo viaje</a>
    <a href="logout.php">Desconectar <?php echo $_SESSION['nombre'] ?></a>
</body>
</html>