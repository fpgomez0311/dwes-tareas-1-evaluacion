<?php

require 'funciones.php';
session_start();
if (!isset($_SESSION['id']))
{
    header("Location: login.php");
    die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reservar un viaje</title>
</head>
<body>
    <h2>Hola <?php echo $_SESSION['nombre'] ?> - Reserva tu viaje</h2>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
     <label for="viajes">Elige el viaje:</label>
     <select name="viajes">
     <?php
     $viajes = obtenerViajes();
     foreach($viajes as $viaje){
        foreach($viaje as $info => $valor){
            foreach($valor as $nombre => $precio){
             echo "<option value='".$info."'>".$nombre." (".$precio.")</option>";
         }
        }
    }
     ?>
     </select><br>
     <label for="personas">Número de personas</label>
     <input type="text" id="personas" name="personas" min="0"><br>
     <br>
     <input type="submit" id="reservar" name="reservar" value="Reservar"><br><br>
     <a href="viajes.php">Volver al listado de viajes</a><br>
     <a href="reservas_realizadas.php">Ver reservas</a><br>
     <a href="logout.php">Desconectar <?php echo $_SESSION['nombre'] ?></a>
    </form>
</body>
</html>

<?php

if(isset($_POST["reservar"])){
    $idcliente = $_SESSION['id'];
    $idviaje = $_POST['viajes'];
    $numpersonas = $_POST['personas'];
    reservarViaje($idcliente,$idviaje,$numpersonas);
}


?>