<?php

session_start();
if (!isset($_SESSION['visitas'])) {
  $_SESSION['visitas'] = 0;
} else {
  $_SESSION['visitas']++;
}

if($_SESSION['visitas'] == 0){
    echo "El usuario no ha visitado la página aún.";
}else{
    echo "El usuario ha visitado la página ". $_SESSION['visitas']." veces.";
}


?>