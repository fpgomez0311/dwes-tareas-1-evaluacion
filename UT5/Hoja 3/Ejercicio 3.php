<?php

require 'funciones.php';
    session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar sesión en el funicular</title>
</head>
<body>
    <h1>Iniciar sesión</h1>
    <form name="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="usuario">Usuario: </label>
    <input type="text" id="usuario" name="usuario"><br>
    <label for="pass">Contraseña: </label>
    <input type="password" id="pass" name="pass"><br>
    <br>
    <input type="submit" id="comprobar" name="comprobar" value="Iniciar sesión"><br><br>
    <input type="submit" id="borrar" name="borrar" value="Borrar historial visitas">
    </form>
</body>
</html>

<?php
    $visitas = array();

if(isset($_POST["comprobar"])){
    $usuario = $_POST["usuario"];
    $pass = $_POST["pass"];
    if(inicioSesion($usuario,$pass) == true){
       $_SESSION["usuario"] = $usuario;
       if($_SESSION["usuario"] == $usuario){
        if(!isset($_SESSION["visitas"])){
            $_SESSION["todas"] = array();
            echo "Bienvenido por primera vez a la página del funicular";
            $tiempo = date('m/d/Y h:i:s a', time());
            $_SESSION['visitas'] = $tiempo;
            array_push($_SESSION["todas"],$_SESSION['visitas']);
        }else{
            echo "Tus últimas visitas fueron en:<br>";
            for($i = 0; $i < count($_SESSION["todas"]);$i++){
                echo $_SESSION["todas"][$i]."<br>";
            }
            $tiempo = date('m/d/Y h:i:s a', time());
            $_SESSION['visitas'] = $tiempo;
            array_push($_SESSION["todas"],$_SESSION['visitas']);
        }
    }
    }else{
        echo "No se ha podido iniciar sesión.";
    }
}
if(isset($_POST["borrar"])){
    session_destroy();
    echo "Se han borrado correctamente las visitas almacenadas.";
}

?>